import java.util.*;
class demo{
	public static void main(String args[]){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Row=");
		int row=sc.nextInt();
		for(int i=1;i<=row;i++){
			for(int space=1;space<=row-i;space++){
				System.out.print(" \t");
			}
			int num=1;
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t ");
				num++;
			}
			System.out.println();
		}
	}
}
