import java.io.*;
class sp6{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Number Of Row=");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
				for(int sp=1;sp<=i;sp++){
				System.out.print("\t");
			}
			for(int j=row;j>=i;j--){
				System.out.print("*\t");
			}
			System.out.println();
		}
	}
}
