import java.io.*;
class space6{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Row=");
		int row=Integer.parseInt(br.readLine());
		int num=3;
		for(int i=row;i>=1;i--){
			for(int sp=1;sp<=row-i;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=i;j++){
				System.out.print(num+"\t");
			}
			num--;
			System.out.println();
		}
	}
}
