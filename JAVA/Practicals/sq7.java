import java.io.*;
class sq7{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Row=");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print("#"+"\t");

				}
				else if(j%2==1){
					System.out.print("C"+"\t");
				}
				else{
					System.out.print("#"+"\t");
				}

			}
			System.out.println();
		}
	}
}
