import java.io.*;
class sq3{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Row=");
		int row=Integer.parseInt(br.readLine());
		System.out.println();
		int num=row;

		for(int i=1;i<=row;i++){  
			int ch=64+num;
			for(int j=1;j<=row;j++){
				if(j==1){
					System.out.print((char)ch+"\t");
					ch++;
				}
				else{
					System.out.print(num+"\t");
				}
				num++;
			}
			System.out.println();
		}
	}
}
