import java.io.*;
class Program2{
		public static void main(String []args)throws IOException{
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			int row=Integer.parseInt(br.readLine());

			int col=0;
			for(int i=1;i<row*2;i++){

				if(i<=row){
					col++;
				}
				else{
					col--;
				}
				for(int j=1;j<=col;j++){
					System.out.print(j+"\t");
				}
			System.out.println();
		}
	}
}
