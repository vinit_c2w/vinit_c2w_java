import java.util.*;
class P10{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		int space=0;
		int num=0;
		for(int i=1;i<row*2;i++){
			int temp=0;
			if(i<=row){
				space=row-i;
				num++;
				temp=row-i+1;
			}
			else{
				space=i-row;
				num--;
				temp=i-row+1;
			}
			for(int sp=1;sp<=space;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=num;j++){
		
				System.out.print((char)(64+temp)+"\t");
				temp++;
			}
			System.out.println();
		}
	
	}
}
