import java.io.*;
class Program10{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0;i<arr.length;i++){
			int fact=1;
			int num=1;
			while(num<=arr[i]){
				fact=fact*num;
				num++;	
			}
			System.out.println(fact);
		}
	}
}
