import java.util.Scanner;

public class arr9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter size: ");
        int size = scanner.nextInt();

        
        int[] arr = new int[size];

        
        System.out.println("Enter elements:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        
        int min = arr[0];
        for (int i = 1; i < size; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
        }

        
        System.out.println("Minimum element in the array: " + min);

        scanner.close();
    }
}

