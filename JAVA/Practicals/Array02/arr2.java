import java.util.Scanner;

class arr2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter size: ");
        int size = scanner.nextInt();

        int[] arr = new int[size];

        System.out.print("Enter elements: ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        int divisibleByThreeSum = 0;
        System.out.print("Elements divisible by 3: ");
        for (int num : arr) {
            if (num % 3 == 0) {
                System.out.print(num + " ");
                divisibleByThreeSum += num;
            }
        }

        System.out.println("\nSum of elements divisible by 3: " + divisibleByThreeSum);
    }
}


