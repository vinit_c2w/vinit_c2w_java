import java.util.Scanner;

public class arr6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter size: ");
        int size = scanner.nextInt();

        
        int[] arr = new int[size];

        
        System.out.println("Enter elements:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        
        int oddIndexedProduct = 1;
        for (int i = 1; i < size; i += 2) {
            oddIndexedProduct *= arr[i];
        }

        
        System.out.println("Product of odd-indexed elements: " + oddIndexedProduct);

        scanner.close();
    }
}

