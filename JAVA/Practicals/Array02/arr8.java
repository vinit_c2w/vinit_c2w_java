import java.util.Scanner;

public class arr8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Enter size: ");
        int size = scanner.nextInt();

     
        int[] arr = new int[size];

        
        System.out.println("Enter elements:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        
        System.out.print("Elements greater than 5 and less than 9: ");
        for (int num : arr) {
            if (num > 5 && num < 9) {
                System.out.print(num + " ");
            }
        }

        scanner.close();
    }
}

