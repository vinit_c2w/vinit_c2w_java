import java.util.Scanner;

public class arr7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

     
        System.out.print("Enter size: ");
        int size = scanner.nextInt();

        
        int[] arr = new int[size];

      
        System.out.println("Enter elements:");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

 
        if (size % 2 == 0) {
            System.out.print("Alternate elements in the array: ");
            for (int i = 0; i < size; i += 2) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        } else {
            System.out.print("Array: ");
            for (int num : arr) {
                System.out.print(num + " ");
            }
            System.out.println();
        }

        scanner.close();
    }
}

