import java.util.Scanner;

public class arr3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter size: ");
        int size = scanner.nextInt();
        scanner.nextLine();

        char[] arr = new char[size];

        System.out.print("Enter elements: ");
        String input = scanner.nextLine();
        for (int i = 0; i < Math.min(size, input.length()); i++) {
            arr[i] = input.charAt(i);
        }

        System.out.print("Indices of vowels: ");
        for (int i = 0; i < size; i++) {
            char ch = arr[i];
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ||
                ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U') {
                System.out.print(i + " ");
            }
        }

        scanner.close();
    }
}


