import java.util.Scanner;

public class arr1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter size: ");
        int size = scanner.nextInt();

       
        int[] arr = new int[size];

        
        System.out.print("Enter elements = ");
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }

        
        int evenCount = 0;
        System.out.print("Even numbers: ");
        for (int num=0;num<arr.length;num++) {
            if (num % 2 == 0) {
                System.out.print(num + " ");
                evenCount++;
            }
        }

        
        System.out.println("\nTotal even numbers: " + evenCount);

        scanner.close();
    }
}

