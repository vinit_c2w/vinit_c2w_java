import java.io.*;

class arr10 {
    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter The Size=");
        int size = Integer.parseInt(br.readLine());

        int arr[] = new int[size];

        System.out.println("Enter The Numbers=");

        for (int i = 0; i < size; i++) {
            arr[i] = Integer.parseInt(br.readLine());
        }

        System.out.println("Prime Numbers:");

        for (int j = 0; j < size; j++) {
            boolean isPrime = true;

            // Check if arr[j] is less than or equal to 1
            if (arr[j] <= 1) {
                isPrime = false;
            } else {
                // Check divisibility of arr[j] by numbers from 2 to arr[j] - 1
                for (int k = 2; k <= arr[j] - 1; k++) {
                    if (arr[j] % k == 0) {
                        isPrime = false;
                        break; // If divisibility found, exit loop
                    }
                }
            }

            // If isPrime is still true, arr[j] is a prime number
            if (isPrime) {
                System.out.println(arr[j]);
            }
        }
    }
}
