import java.io.*;
class space10{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Row=");
		int row=Integer.parseInt(br.readLine());

		for(int i=1;i<=row;i++){
			//char ch=65;
			for(int sp=row;sp>=i;sp--){
				System.out.print("\t");
			}
			for(int j=1;j<=i*2-1;j++){
				System.out.print("*"+"\t");
			}
			System.out.println();
		}
	}
}
