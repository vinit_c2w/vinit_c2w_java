

import java.util.Scanner;

 class pattern9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        
        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
        
        int num = 19;
        
        
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= rows - i + 1; j++) {
                System.out.print(num + " ");
                num -= 2;
            }
            System.out.println();
            
            
            num = 2 * (rows - i) + 1;
        }
        
        scanner.close();
    }
}
