import java.util.*;
 class pattern8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
        
        int num = rows;
        char letter = ' ';

    
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < num; j++) {
                System.out.print(num-j +" ");
            }
            num--;

            if (i == 0) {
                letter = 'C';
            } else {
                letter++;
            }

            for (int k = 0; k < i + 1; k++) {
                System.out.print(letter-k +" ");
            }
            System.out.println();
        }
        
    }
}

