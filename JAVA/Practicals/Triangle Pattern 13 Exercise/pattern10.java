import java.util.Scanner;

public class pattern10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        
        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();
        
        char startCharUpper = 'D';
        char startCharLower = 'c';
        
        
        for (int i = 1; i <= rows; i++) {
            char upperChar = startCharUpper;
            char lowerChar = startCharLower;
            
            
            for (int j = 1; j <= rows - i + 1; j++) {
                System.out.print(upperChar + " ");
                upperChar--;
            }
            
            
            for (int k = 1; k <= rows - i + 1; k++) {
                System.out.print(lowerChar + " ");
                lowerChar--;
            }
            
            System.out.println();
            
            
            startCharUpper--;
            startCharLower--;
        }
        
        scanner.close();
    }
}
