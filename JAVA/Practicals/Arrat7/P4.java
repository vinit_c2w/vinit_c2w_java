import java.io.*;
class P4{
	public static void main(String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int row=Integer.parseInt(br.readLine());
		int col=Integer.parseInt(br.readLine());
		int temp=1;
		int arr[][]=new int[row][col];
		for(int i=0;i<row;i++){
			for(int j=0;j<col;j++){
				arr[i][j]=Integer.parseInt(br.readLine());
			}
		}
		for(int i=0;i<row;i++){
			int sum=0;
			if(temp%2==1){
                        	for(int j=0;j<col;j++){
					sum=sum+arr[i][j];
				}
				System.out.println("The sum of row "+temp+" is "+sum);

                        }
		 temp++;	
                }
	  }
}	
