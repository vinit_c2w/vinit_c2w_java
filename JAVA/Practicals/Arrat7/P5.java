import java.io.*;
class P5{
	public static void main(String []args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
				int row=Integer.parseInt(br.readLine());
				int col=Integer.parseInt(br.readLine());
				int arr[][]=new int[row][col];
				for(int i=0;i<row;i++){
					for(int j=0;j<col;j++){
						arr[i][j]=Integer.parseInt(br.readLine());
					}
				}
				int num=1;
				int temp=0;
				while(temp<row){
					int sum=0;
					for(int i=0;i<row;i++){
						for(int j=0;j<col;j++){
							if(j==temp){
								sum=sum+arr[i][j];
							}
							else{
								continue;
							}
						}
					}
					System.out.println("Sum of coloums "+num+" = "+sum);
					temp++;
					num++;
				}
			}
	}

