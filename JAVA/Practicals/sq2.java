import java.io.*;
class sq2{
	public static void main(String args[])throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Enter The Row=");
			int row=Integer.parseInt(br.readLine());
			System.out.println();
			int num=row;
			for(int i=1;i<=row;i++){
				for(int j=1;j<=row;j++){
					if(num%3==0){
						System.out.print(num*num+"\t");
					}
					else{
						System.out.print(num+"\t");
					}
					num++;
				}
				System.out.println();
			}
	}
}
